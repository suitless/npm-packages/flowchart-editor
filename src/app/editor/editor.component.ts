import { Component, OnInit } from '@angular/core';
import { state, methods } from 'flowchart-editor-lib/public-api';
import editorTheme from './models/editor-theme';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.startEditor();
  }

  private startEditor() {
    // let setDialogmethod = this.setDialogState;
    // state.dialog.registerListener((val) => {
    //     setDialogmethod(val);
    // });

    // let setActiveFormatBarmethod = this.setActiveFormatBar;
    // state.activeFormatBar.registerListener((val) => {
    //     setActiveFormatBarmethod(val);
    // });

    // let setSelectedCellmethod = this.setnewSelectedCell;
    // state.newCell.registerListener((val) => {
    //     setSelectedCellmethod(val);
    // });

    state.theme = editorTheme;
}

}
